import Foundation

// Создайте по 2 enum с разным типом RawValue

enum Speed : String {
    case Low = "Walk"
    case Medium = "Pedestrianism"
    case High = "Run"
}

enum Distance : Int {
    case One = 1
    case Two = 2
    case Three = 3
}

enum Training {
    case Distance(distance: Distance)
    case Speed(speed: Speed)
}

// Создайте несколько enum для заполнения полей стркутуры - анкета сотрудника: enum пол, enum категория возраста, enum стаж

struct Empolyee {

    enum Gender {
        case Male
        case Female
    }
    
    enum Age {
        case Young 
        case Adult 
        case MiddleAged 
    }
    
    enum WorkExperience{
        case None
        case Average
        case Extensive
    }
}

// Создать enum со всеми цветами радуги

enum Rainbow {
    case Red
    case Orange
    case Yellow
    case Green
    case Blue
    case Indigo
    case Violet
}

// Создать функцию, которая содержит массив разных case'ов enum и выводит содержимое в консоль. Пример результата в консоли 'apple green', 'sun red' и т.д.

func Cases (color: Rainbow) {
    let objects = ["apple", "mandarin", "sun", "grass", "ice", "dress", "eggplant"]
    switch color {
        case .Red:
            print("\(objects[0]) red")
        case .Orange:
            print("\(objects[1]) orange")
        case .Yellow:
            print("\(objects[2]) yellow")
        case .Green:
            print("\(objects[3]) green")
        case .Blue:
            print("\(objects[4]) blue")
        case .Indigo:
            print("\(objects[5]) indigo")
        case .Violet:
            print("\(objects[6]) violet")
    }
}

Cases(color: Rainbow.Green)
Cases(color: Rainbow.Blue)


// Создать функцию, которая выставляет оценки ученикам в школе, на входе принимает значение enum Score: String {<Допишите case'ы} и выводит числовое значение оценки

enum Marks : String {
    case Great
    case Good
    case Satisfactorily
    case Weakly
    case Unsatisfactory
}

let students = ["Tom", "Wick", "Barney", "El"]

func StudentMark(mark: Marks, student: String) {
    switch mark {
        case .Great:
            print("\(student) grade 5")
        case .Good:
            print("\(student) grade 4")
        case .Satisfactorily:
            print("\(student) grade 3")
        case .Weakly:
            print("\(student) grade 2")
        case .Unsatisfactory:
            print("\(student) grade 1")
    }
}

StudentMark(mark: Marks.Great, student: students[2])
StudentMark(mark: Marks.Weakly, student: students[3])

// Создать метод, которая выводит в консоль какие автомобили стоят в гараже, используйте enum

enum Cars {
    case Supra
    case Skyline
    case M4
    
    func Garage(car: Cars) {
        switch car {
            case .Supra:
                print("Toyota Supra in garage")
            case .Skyline:
                print("Nissan Skyline in garage")
            case .M4:
                print("BMW M4 in garage")
        }
    }
}

let car = Cars.Skyline
car.Garage(car: car)