import Foundation

// Создать класс родитель и 2 класса наследника

class Car {
    var speed: String?
    var weight: Double?
}

class TypeCar: Car {
    var typeOfCar: String?
}
class Tuning: Car  {
  var hasTuning = false
}

// Создать класс *House* в нем несколько свойств - *width*, *height* и несколько методов: *create*(выводит площадь),*destroy*(отображает что дом уничтожен)
class House {
    var width: Int
    var height: Int
    
    init(width: Int, height: Int) {
        self.width = width 
        self.height = height
    }
    

    func create() {
        print("Площадь дома: \(width*height)")
    }
    func destroy() {
        print("Дом уничтожен")
    }
}

var house = House(width: 5, height: 3)
house.create()
house.destroy()

// Создайте класс с методами, которые сортируют массив учеников по разным параметрам

class Students {


    func sortDescending(array: [String]) -> [String] {
        return array.sorted(by: {$0.count  > $1.count })
    }
    func sortAscending(array: [String]) -> [String] {
        return array.sorted(by: {$0.count  < $1.count })
    }
}

let students = ["Wick", "Barney", "El", "Philipp", "Tom"]
let studentsFunc = Students()
let stundentsSortAscending = studentsFunc.sortDescending(array: students)
print(stundentsSortAscending)

// Написать свою структуру и класс, и пояснить в комментариях - чем отличаются структуры от классов
class CLass { var data : String; init(data: String) {self.data = data} }
var class1  = CLass(data: "A")
var class2 = class1
class1.data = "B"
print(class2.data)
print(class1.data)

struct Struct {var data: String; init(data: String) {self.data = data} }
var struct1  = Struct(data: "A")
var struct2 = struct1
struct1.data = "B"
print(struct1.data)
print(struct2.data)
/* Класс может наследовать характеристики другого класса, есть возможность деиницилизации экземпляра класса;
   Структура является Value Type, а класс является Reference Type. 
   Таким образом:
    если изменить экземпляр класса class1, то значения изменится и в class2, так как имеем сылочный тип
    если изменить значение в struct1, то значения не изменится в struct2 */
   
// Напишите простую программу, которая отбирает комбинации в покере из рандомно выбранных 5 карт
// Сохраняйте комбинации в массив
// Если выпала определённая комбинация - выводим соответствующую запись в консоль
// Результат в консоли примерно такой: 'У вас бубновый стрит флеш'.

class Combinations {
   let ranks = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
   
   enum CardSuits: String, CaseIterable {
    case Hearts = "Червовый"
    case Spades = "Пиковый"
    case Diamonds = "Бубновный"
    case Clubs = "Трефовый"
   }
   enum Combinations: String {
       case HighCard = "Старшая карта"
       case Pair = "Пара"
       case TwoPairs = "Две пары"
       case ThreOfKind = "Тройка"
       case Straight = "Стрит"
       case Flush = "Флэш"
       case FullHouse = "Фулл-Хаус"
       case FourOfKind = "Каре"
       case StraightFlush = "Стрит флеш"
       case RoyalFlush = "Роял флеш"
   }
   func createCombination() -> [(suit: String,rank: Int)]  {

       var suitsRanks : [(suit: String,rank: Int)] = []
       let suits = CardSuits.allCases
       for i in 0..<ranks.count {
              for j in 0..<suits.count {
                  suitsRanks += [(suit: suits[j].rawValue, rank: ranks[i])]
              }
        }
        var combination: [(suit: String,rank: Int)] = []
        for i in 0...4 {
           let index = Int.random(in: 0..<suitsRanks.count)
           let card = suitsRanks[index]
           suitsRanks.remove(at: index)
           combination.append(card)
        }
        return combination
   }
   
   func game(combination: [(suit: String,rank: Int)]) {
       // Сортируем карты в комбинации по рангу карт
       let combination = combination.sorted { (n1: (suit: String,rank: Int), n2: (suit: String,rank: Int))  -> Bool in
                                                 return n1.rank < n2.rank }
        print(combination)
       // Проверка на флэш
       var result = Combinations.Flush
       for i in 1..<combination.count {
           if combination[i-1].suit != combination[i].suit {
              result = Combinations.HighCard
           }
       }
       if result == Combinations.Flush {
           result = Combinations.StraightFlush
           for i in 1..<combination.count {
               if combination[i].rank - combination[i-1].rank != 1 {
                    result = Combinations.Flush
                }
            }
        }
        if result == Combinations.StraightFlush {
            if combination[0].rank >= 10 {
                result = Combinations.RoyalFlush
            }
        }
        if result == Combinations.HighCard {
           result = Combinations.Straight 
           for i in 1..<combination.count {
               if combination[i].rank - combination[i-1].rank != 1   {
                     result = Combinations.HighCard
                }   
            }
        }
        var count = [0,0,0,0,0]
        var countOfPair = 0
        for i in 0..<combination.count {
            for j in 0..<combination.count {
                if combination[i].rank == combination[j].rank && j != i {
                count[i] += 1
                }
             }
        }
        print(count)
        for i in 0..<count.count {
            if count[i] == 3 {
                result = Combinations.FourOfKind
                break
            }
            if count[i] == 2 {
                result = Combinations.ThreOfKind
            }
            if count[i] == 1 {
                countOfPair += 1
            }
            if countOfPair == 4 {
                result = Combinations.TwoPairs
            }
        }
        if countOfPair > 0 && result == Combinations.ThreOfKind {
                result = Combinations.FullHouse
        } 
        else {
           if countOfPair == 4 {
                result = Combinations.TwoPairs 
            }
            else {
                if countOfPair == 2 {
                    result = Combinations.Pair
                }
            }
        } 

        switch result {
           case .Flush:
           print("У вас \(combination[0].suit) \(Combinations.Flush.rawValue)")
           case .StraightFlush:
           print("У вас \(combination[0].suit) \(Combinations.StraightFlush.rawValue)")
           case .RoyalFlush:
           print("У вас \(combination[0].suit) \(Combinations.RoyalFlush.rawValue)")
           case .Straight: 
           print("У вас  \(Combinations.Straight.rawValue)")
           case .Pair:
           print("У вас  \(Combinations.Pair.rawValue)")
           case .FullHouse:
           print("У вас  \(Combinations.FullHouse.rawValue)")
           case .FourOfKind:
           print("У вас  \(Combinations.FourOfKind.rawValue)")
           case .ThreOfKind:
           print("У вас  \(Combinations.ThreOfKind.rawValue)")
           case .TwoPairs:
           print("У вас \(Combinations.TwoPairs.rawValue)")
           default: print("У вас \(combination[4].rank) \(Combinations.HighCard.rawValue)")
        }
    }
}

let combinatios = Combinations()
combinatios.game(combination: combinatios.createCombination())
