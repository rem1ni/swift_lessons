import Foundation

// Написать сортировку массива с помощью замыкания, в одну сторону, затем в обратную
// Вывести результат в консоль
let numbers = [5, 8, 1, 4, 113, 13, 65, 67]

let ascending = numbers.sorted(by: { (n1: Int, n2: Int) -> Bool in
    return n1 < n2 })
    
print(ascending)

let descending = numbers.sorted(by: {$0 > $1})
    
print(descending)

// Создать метод, который принимает имена друзей, после этого имена положить в массив

var names = [String]()

func addName(name : String) {
    names.append(name)
}

addName(name: "Wick")
addName(name: "Barney")
addName(name: "El")
addName(name: "Philipp")
addName(name: "Tom")
print(names)
// Массив отсортировать по количеству букв в имени
names = names.sorted(by: {$0.count < $1.count})
print(names)

//Создать словарь (Dictionary), где ключ - кол-во символов в имни, а в значении - имя друга
var dictionaryName = [Int: String]()

for name in names {
    dictionaryName[(name.count)] = name
}
print(dictionaryName)

//Написать функцию, которая будет принимать ключ, выводить полученный ключ и значение
func getName(key : Int) {
    if dictionaryName[key] != nil {
     print("Key: \(key), Name: \(dictionaryName[key])")   
    }
}
getName(key: 2)
getName(key: 6)

//Написать функцию, которая принимает 2 массива (один строковый, второй - числовой) и проверяет их на пустоту: если пустой - то добавьте любое значения и выведите массив в консоль
var arrayInt = [Int]()
var arrayString = [String]()
let appendString = "string"
let appendInt = 1
func addValue(array1: inout [String], array2: inout [Int]) {
    if array1.isEmpty {
        array1.append(appendString)
        print(array1)
    }
    if array2.isEmpty {
        array2.append(appendInt)
        print(array2)
    }
}
addValue(array1: &arrayString, array2: &arrayInt)