import Foundation

// Реализовать структуру IOSCollection и создать в ней copy on write
struct IOSCollection {
    var number = 1
}

class Ref<T> { 
    var value : T
    init (value: T) {
        self.value = value
    } 
}

struct Container <T> { 
    var ref : Ref<T> 
    init(value: T) {
        self.ref = Ref(value: value) 
    }

    var value : T {
        get { 
            ref.value
        }

        set { 
        // Если есть уникальная ссылка на объект, то меняем значение ссылки, если нет, то создаем новый экземпляр
            guard(isKnownUniquelyReferenced(&ref)) else { 

                ref = Ref(value: newValue)
                return
            }
            ref.value = newValue
        }
    }
}
var id = IOSCollection()
var container1 = Container(value: id)
var container2 = container1

/* Создать протокол *Hotel* с инициализатором, который принимает roomCount, 
после создать class HotelAlfa добавить свойство roomCount и подписаться на этот протокол */

protocol Hotel {
   init (roomCount : Int)
}

class HotelAlfa : Hotel {
    var roomCount : Int

    required init (roomCount: Int) {
        self.roomCount = roomCount
    }
}

print(HotelAlfa(roomCount: 1).roomCount)

/* Создать protocol GameDice у него {get} свойство numberDice далее нужно расширить Int так, 
чтобы когда мы напишем такую конструкцию 'let diceCoub = 4 diceCoub.numberDice' в консоле мы 
увидели такую строку - 'Выпало 4 на кубике' */

protocol GameDice {
    var numberDice : String { get }
}

extension Int : GameDice {
    var numberDice : String {
        return "Выпало \(self) на кубике"
    }
}

let diceCoub = 4
print(diceCoub.numberDice)

/* Создать протокол с одним методом и 2 свойствами одно из них сделать явно optional, 
создать класс, подписать на протокол и реализовать только 1 обязательное свойство */

@objc protocol CarProtocol {
    @objc optional var speed : Int { get }
    var model : String { get }
    
    func printWeighht()

}

 class Car: CarProtocol {
    var model : String
    init (model: String) {
        self.model = model
    }
    func printWeighht() {
        print("Модель  машины: \(model)")
    }
}

let car = Car(model: "BMW E3")
car.printWeighht()
/* Создать 2 протокола: со свойствами время, количество кода и функцией writeCode(platform: Platform, numberOfSpecialist: Int); 
и другой с функцией: stopCoding(). Создайте класс: Компания, у которого есть свойства - количество программистов, 
специализации(ios, android, web) */
class Platform {
    var language: String
    init (language: String) {
        self.language = language
    }
}

protocol WriteCode {
    var time: String { get }
    var code: Int { get }
    func writeCode(platform: Platform, numberOfSpecialist: Int)
}

protocol StopCoding {
    func stopCoding()
}

class Company: WriteCode, StopCoding {
    var time: String
    var code: Int
    var platform: Platform
    var numberOfSpecialist: Int
    init (time: String, code: Int, platform: Platform, numberOfSpecialist: Int) {
        self.time = time
        self.code = code
        self.platform = platform
        self.numberOfSpecialist = numberOfSpecialist
    }
    func writeCode(platform: Platform, numberOfSpecialist: Int) {
        print("Разработка началась. Пишем код для \(platform.language), на \(code) строк, используя \(numberOfSpecialist) специалистов за \(time)")
    }
    func stopCoding() {
        print("Работа закончена. Сдаю в тестирование")
    }
}

let platform = Platform(language: "ios")
let company = Company(time: "20 дней", code: 2111, platform: platform, numberOfSpecialist: 5)
company.writeCode(platform: company.platform, numberOfSpecialist: company.numberOfSpecialist)
company.stopCoding()