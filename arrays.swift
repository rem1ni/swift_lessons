import Foundation

// Создать массив элементов 'кол-во дней в месяцах' содержащих количество дней в соответствующем месяце
let days: [Int] =  [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

// Создать массив элементов 'название месяцов' содержащий названия месяцев
let months = ["January", "February", "March", "April", "May", 
               "June", "July", "August", "September", "October", "November", "December"]

// Используя цикл for и массив 'кол-во дней в месяцах' выведите количество дней в каждом месяце (без имен месяцев)
for i in days {
    print(i)
}

// Используйте еще один массив с именами месяцев, чтобы вывести название месяца + количество дней
for i in 0..<months.count {
    print("\(days[i]) days in \(months[i])")
}

// Сделайте тоже самое, но используя массив tuples (кортежей) с параметрами (имя месяца, кол-во дней)
let daysInMonths = [("January", 31), ("February", 28), ("March", 31), ("April", 30), ("May", 31), ("June", 30), ("July", 31), 
                    ("August", 31), ("September", 30), ("October", 31), ("November", 30), ("December", 31)]
for i in daysInMonths {
    print(i)
}

// Сделайте тоже самое, только выводите дни в обратном порядке (порядок в массиве не менять)
for i in 0..<daysInMonths.count {
    print(daysInMonths[daysInMonths.count - 1 - i])
}

// Для произвольно выбранной даты (месяц и день) посчитайте количество дней до этой даты от начала года
var myDate = ("May", 31)
var (month, day) = myDate
var countDays = 0
for i in 0..<months.count {
    if (month != months[i]) {
        countDays += days[i]
    }
    else {
        countDays += day-1
        break
    }
}
print(countDays)